import urllib.request # librería que usamos para

class Robot:    #Clase robot
    def __init__(self, _url):      #Construimos el objeto
        self.url = _url     #Definimos la variable url
        self.content = None     #
        print("Objeto creado")

    def retrieve(self):     # Metodo retrieve
        if self.content == None:     # Si no hay
            print(f"Descargando " + self.url)    # Mostramos mensaje
            r = urllib.request.urlopen(self.url)     #
            self.content = r.read().decode('utf-8')      #

    def show(self):
        self.retrieve()
        print(self.content)

    def get_content(self):
        self.retrieve()
        return self.content

class Cache:
    def __init__(self):
        self.cache = {}
        print("Objeto creado")

    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url)
            robot.retrieve()
            self.cache[url] = robot.get_content()

    def show(self, url):
        self.retrieve(url)
        print(self.cache[url])

    def show_all(self):
        print("Lista de urls en caché: ")
        for url in self.cache:
            print(url)

    def get_content(self, url):
        self.retrieve(url)
        return self.cache[url]

if __name__ == "__main__":
    robot1 = Robot('http://gsyc.urjc.es/')
    robot2 = Robot("https://www.wikipedia.org")

    robot1.retrieve()
    robot1.show()
    print("\n")
    robot2.show()

    cache1 = Cache()
    cache2 = Cache()
    cache1.retrieve('http://gsyc.urjc.es/')
    cache1.retrieve("https://www.wikipedia.org")
    cache1.show_all()
    print("\n")
    print(cache1.get_content('http://gsyc.urjc.es/'))
    print("\n")
    print(cache2.get_content("https://www.wikipedia.org"))